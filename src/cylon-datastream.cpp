/**
 * @file cylon-datastream.cpp
 * @date 30-May-2019
 */

#include "orx.h"

/*
 * This is a basic code template to quickly and easily get started with a project or tutorial.
 */


/** Init function, it is called when all orx's modules have been initialized
 */
orxSTATUS orxFASTCALL Init()
{
    // Display a small hint in console
    orxLOG("\n* This template project creates a viewport/camera couple and an object"
    "\n* You can play with the config parameters in ../data/config/cylon-datastream.ini"
    "\n* After changing them, relaunch the executable to see the changes.");

	orxMath_InitRandom((orxS32)orxSystem_GetRealTime()); 

    // Create the viewport
    orxViewport_CreateFromConfig("Viewport");

    // Create the object
    orxObject_CreateFromConfig("Scene");

    // Done!
    return orxSTATUS_SUCCESS;
}

/** Run function, it is called every clock cycle
 */
orxSTATUS orxFASTCALL Run()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

	orxINPUT_TYPE inputType;
	orxENUM buttonID;
	orxFLOAT value;
	orxInput_GetActiveBinding(&inputType, &buttonID, &value);
	if (inputType == orxINPUT_TYPE_KEYBOARD_KEY ||
		//inputType == orxINPUT_TYPE_MOUSE_AXIS ||
		inputType == orxINPUT_TYPE_MOUSE_BUTTON
	) {
		//eResult = orxSTATUS_FAILURE;
		//orxLOG("Quit on: %d ID: %d Value: %f", inputType, buttonID, value); // 4 0 ?
	}
	

    // Should quit?
    if(orxInput_IsActive("Quit"))
    {
        // Update result
        eResult = orxSTATUS_FAILURE;
    }

    // Done!
    return eResult;
}

/** Exit function, it is called before exiting from orx
 */
void orxFASTCALL Exit()
{
    // Let Orx clean all our mess automatically. :)
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS orxFASTCALL Bootstrap()
{
    // Add a config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Set the bootstrap function to provide at least one resource storage before loading any config files
    orxConfig_SetBootstrap(Bootstrap);

    // Execute our game
    orx_Execute(argc, argv, Init, Run, Exit);

    // Done!
    return EXIT_SUCCESS;
}
